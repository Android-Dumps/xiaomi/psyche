#
# Copyright (C) 2022 The Android Open Source Project
# Copyright (C) 2022 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_psyche-user
add_lunch_combo omni_psyche-userdebug
add_lunch_combo omni_psyche-eng
