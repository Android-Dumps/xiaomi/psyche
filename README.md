## qssi-user 12
12 SKQ1.211230.001 V13.0.1.0.SLDMIXM release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: psyche
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.211230.001
- Incremental: V13.0.1.0.SLDMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/psyche_global/psyche:12/RKQ1.211001.001/V13.0.1.0.SLDMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211230.001-V13.0.1.0.SLDMIXM-release-keys
- Repo: xiaomi/psyche
